# Cartes uniques
Cartes uniques totales : 28
## Totaux par catégorie
- **Adjuvant**: 2 (Mobilisation citoyenne, Anticor & friends) – imprimées 2 fois.
- **Objectif**: 4 (Hôpital public et retraites, Confiance en la démocratie, Éducation et recherche, Transition écologique) 
- **Révélation**: 3 (Lanceur d'alerte, Journaliste d'investigation, Article 40) – imprimées 2 fois.
- **Délit**: 7 (Concussion, Détournement de fonds, Favoritisme, Prise illégale d'intérêts, Corruption, Trafic d'influence, Achat de voix) 
- **Parade**: 6 (Procédure baîllon, Concentration des medias, Sous-financement de la justice, Secret des affaires, Classement sans suite, Convention judiciaire d’intérêt public) 
- **Procédure**: 4 (Procureur, Juge d'instruction, Police judiciaire, Jugement) – imprimées 3 fois.
- **Temps**: 2 (Un an, Deux ans) 
Un an imprimée 10 fois : Deux ans imprimée 5 fois
## Totaux par camp
- Camp éthique: 13 cartes uniques
- Camp corruption: 15 cartes uniques
# Cartes imprimées
Cartes totales imprimées : 54
## Totaux par camp
- Camp éthique: 26 cartes uniques
- Camp corruption: 28 cartes uniques