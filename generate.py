# Script pour générer un fichier JSON de données de cartes FICHIER_CARTES
# et un fichier JSON contenant les cartes répétées en fonctions des chiffres indiqués
# dans CARDINALITES

import json
from unidecode import unidecode

FICHIER_CARTES = "cartes.json"

ECRASER = False

CARDINALITES = {
    "Délit": 1,
    "objectifs": 2,
    "Procédure": 3,
    "Révélation": 1,
    "Parade": 2,
    "adjuvants": 2,
    "Un an": 7,
    "Deux ans": 3,
}


DELITS = [
    ["Concussion", "2 500 000 €", "6", "12"],
    ["Détournement\nde fonds", "5 000 000 €", "6", "12"],
    ["Favoritisme", "1 000 000 €", "6", "12"],
    ["Prise illégale\nd'intérêts", "2 500 000 €", "6", "12"],
    ["Corruption", "5 000 000 €", "7", "20"],
    ["Trafic d'influence", "5 000 000 €", "7", "20"],
    ["Achat de voix", "2 500 000 €", "12", "20"],
]
OBJECTIFS = [
    "Hôpital public",
    "Confiance en\nla démocratie",
    "Éducation\net recherche",
    "Retraites",
    "Transition\nécologique",
]


PROCEDURE = ["Procureur", "Juge\nd'instruction", "Police\njudiciaire", "Jugement"]

REVELATEURS = ["Lanceur d'alerte", "Journaliste\nd'investigation", "Article 40"]

PARADES = [
    "Procédure baîllon",
    "Concentration\ndes medias",
    "Sous-financement\nde la justice",
    "Secret\ndes affaires",
    "Classement\nsans suite",
    "Convention judiciaire\nd’intérêt public"
]

ADJUVANTS = ["Mobilisation\ncitoyenne", "Anticor\n& friends"]


def simplifier(titre):
    simple = unidecode(titre.replace("\n", "_"))
    simple = "".join([e if e.isalnum() else "_" for e in simple])
    return simple.lower()


try:
    with open(FICHIER_CARTES) as f:
        cartes = json.load(f)
except:
    cartes = dict()

for adjuvant in ADJUVANTS:
    carte = dict()
    carte["camp"] = "éthique"
    carte["categorie"] = "Adjuvant"
    carte["titre"] = adjuvant
    carte["icone"] = "adjuvant.jpeg"
    carte["image"] = f"{simplifier(adjuvant)}.jpeg"
    carte[
        "texte"
    ] = f"Texte d'exemple\nsur plusieurs lignes\n pour expliquer la carte {adjuvant}."
    if (carte["titre"] not in cartes) or ECRASER:
        cartes[carte["titre"]] = carte
for objectif in OBJECTIFS:
    carte = dict()
    carte["camp"] = "éthique"
    carte["categorie"] = "Objectif"
    carte["titre"] = objectif
    carte["icone"] = "objectif.jpeg"
    carte["image"] = f"{simplifier(objectif)}.jpeg"
    carte[
        "texte"
    ] = f"Texte d'exemple\nsur plusieurs lignes\n pour expliquer la carte {objectif}."
    if (carte["titre"] not in cartes) or ECRASER:
        cartes[carte["titre"]] = carte
for revelateur in REVELATEURS:
    carte = dict()
    carte["camp"] = "éthique"
    carte["categorie"] = "Révélation"
    carte["titre"] = revelateur
    carte["icone"] = "revelation.jpeg"
    carte["image"] = f"{simplifier(revelateur)}.jpeg"
    carte[
        "texte"
    ] = f"Texte d'exemple\nsur plusieurs lignes\n pour expliquer la carte {revelateur}."
    if (carte["titre"] not in cartes) or ECRASER:
        cartes[carte["titre"]] = carte
for delit in DELITS:
    carte = dict()
    carte["camp"] = "corruption"
    carte["categorie"] = "Délit"
    carte["titre"] = delit[0]
    carte["icone"] = "delit.jpeg"
    carte["image"] = f"{simplifier(delit[0])}.jpeg"
    carte["montant"] = delit[1]
    carte["prescription"] = f"{delit[2]} ans"
    carte["duree_max"] = f"{delit[3]} ans"
    titre = delit[0].replace('\n',' ')
    carte[
        "texte"
    ] = f"Texte d'exemple\nsur plusieurs lignes\n pour expliquer la carte {titre}."
    if (carte["titre"] not in cartes) or ECRASER:
        cartes[carte["titre"]] = carte
for parade in PARADES:
    carte = dict()
    carte["camp"] = "corruption"
    carte["categorie"] = "Parade"
    carte["titre"] = parade
    carte["icone"] = "parade.jpeg"
    carte["image"] = f"{simplifier(parade)}.jpeg"
    carte[
        "texte"
    ] = f"Texte d'exemple\nsur plusieurs lignes\n pour expliquer la carte {parade}."
    if (carte["titre"] not in cartes) or ECRASER:
        cartes[carte["titre"]] = carte
for index, procedure in enumerate(PROCEDURE):
    etapes = len(PROCEDURE)
    etape = f"{index+1}/{etapes}"
    carte = dict()
    carte["camp"] = "éthique"
    carte["categorie"] = "Procédure"
    carte["titre"] = procedure
    carte["etape"] = etape
    carte["icone"] = "procedure.jpeg"
    carte["image"] = f"{simplifier(procedure)}.jpeg"
    carte[
        "texte"
    ] = f"Texte d'exemple\nsur plusieurs lignes\n pour expliquer la carte {procedure}."
    if (carte["titre"] not in cartes) or ECRASER:
        cartes[carte["titre"]] = carte
for temps in ["Un an", "Deux ans"]:
    carte = dict()
    carte["camp"] = "corruption"
    carte["categorie"] = "Temps"
    carte["titre"] = temps
    carte["icone"] = "temps.jpeg"
    carte["image"] = f"{simplifier(temps)}.jpeg"
    carte[
        "texte"
    ] = f"Texte d'exemple\nsur plusieurs lignes\n pour expliquer la carte {temps}."
    if (carte["titre"] not in cartes) or ECRASER:
        cartes[carte["titre"]] = carte

with open(FICHIER_CARTES, "w") as f:
    json.dump(cartes, f, indent=2, ensure_ascii=False)

