# SPDX-FileCopyrightText: 2023 GL75
# SPDX-License-Identifier: Unlicense
# Un script pour générer des cartes pour le jeu A GL75. Les cartes à imprimées sont
# définies dans un fichier JSON (FICHIER_CARTES). Si une représentation graphique en
# existe dans le répertoire bitmaps, alors elle est utilisée, sinon la représentation de
# la carte est générée en SVG à la volée.
# En entrée :
#   - les constantes en début de fichier pour définir :
#     - la mise en page (la taille des cartes, etc.)
#     - Le nombre de fois qu'une catégorie de cartes ou qu'une carte individuelle doit
#       être répétée (dans le dictionnaire CARDINALITES )
#   - un fichier json contenant les données des cartes
#   - un répertoire bitmaps/ contenant les exports bitamps complet des cartes
#   - un répertoire imgs/ contenant uniquement les illustrations des cartes
# En sortie :
#   - une série de fichiers svg et pdf correspondant aux différentes planches à
#     imprimer
#   - un fichier Cartes_JDC.pdf les contenant toutes.
#   - un fichier Resume.md donnant la synthèse des cartes imprimées, par catégorie, etc.
# Pré-requis techniques :
# - Les dépendances sont définies dans le fichier pyproject.toml
# La génération du SVG se fait avec la bibliothèque drawsvg :
# https://github.com/cduck/drawsvg
# La conversion en PDF se fait avec
# https://cairosvg.org/
# La fusion des PDF avec
# https://pypdf.readthedocs.io/en/latest/user/merging-pdfs.html

import json
import os
import drawsvg as draw
import cairosvg
import textwrap
from pypdf import PdfWriter

FICHIER_CARTES = "cartes.json"
FICHIER_RESUME = "Resume.md"

# Paramètres de mise en page

PAGE_WIDTH = 210
PAGE_HEIGHT = 297
MARGIN_MIN_WIDTH = 10
MARGIN_MIN_HEIGHT = 10

CARD_WIDTH = 57
CARD_HEIGHT = 88

CARD_PADDING = 2
GUTTER = 1
CARD_ICON = 7
CARD_HEADER = CARD_ICON + 2*CARD_PADDING
CARD_BODY = 60
CARD_TEXT = 20

DELIT_INFO = CARD_BODY  - (CARD_HEADER + CARD_PADDING)

MONTANT_X_FIN = 30

PRESCRIPTION_X_DEBUT = MONTANT_X_FIN + CARD_PADDING


CARDS_PER_LINE = (PAGE_WIDTH - 2 * MARGIN_MIN_WIDTH) // CARD_WIDTH
LINES_PER_PAGE = (PAGE_HEIGHT - 2 * MARGIN_MIN_HEIGHT) // CARD_HEIGHT
CARDS_PER_PAGE = CARDS_PER_LINE * LINES_PER_PAGE

MARGIN_WIDTH = (PAGE_WIDTH - (CARDS_PER_LINE * CARD_WIDTH))/2

POLICE_HEIGHT = 4

LARGEUR_GRILLE = 0.3
print(f"Page : {PAGE_WIDTH}x{PAGE_HEIGHT}")
print(f"{CARDS_PER_LINE} cartes par ligne, {LINES_PER_PAGE} lignes par page")
SUB_STYLE = "font-style:normal;font-weight:bold;font-family:Ubuntu"
STYLE_CATEGORIE = "font-style:normal;font-weight:bold;font-family:Purisa"
STYLE_TITRE = "font-style:normal;font-weight:bold;font-family:Barlow"
STYLE_TEXTE = "font-style:normal;font-family:Barlow"
# Caractéristiques des cartes

PALETTE = {
    "éthique": "#55AA55",
    "corruption": "#101010",
    "Parade": "#f9e524",
    "Délit": "#e57183",
    "Adjuvant": "#4E9A06",
    "Objectif": "#8ae234",
    "Révélation": "#55AA55",
    "Etape" : "#55AA55",
    "Procédure" : "#88AA22",
    "Temps" : "#AAAAAA"

}


CARDINALITES = {
    "Délit": 1,
    "Objectif": 1,
    "Procédure": 3,
    "Révélation": 2,
    "Parade": 1,
    "Adjuvant": 2,
    "Un an": 10,
    "Deux ans": 5,
}

wrapper = textwrap.TextWrapper(width=40)

def from_bitmap(bitmap_path):
    """
    Crée un objet SVG ne contenant que la bitmap correspond à la carte décrite dans
    l'objet (dictionnaire) carte
    """

    bitmap = draw.Image(
        0,
        0,
        CARD_WIDTH,
        CARD_HEIGHT,
        bitmap_path,
        embed=True,
    )
    group = draw.Group()
    group.append(bitmap)
    return group

def to_svg(carte):
    """
    Crée un objet SVG à partir d'un objet (dictionnaire) carte
    """
    # cadre
    cadre = draw.Rectangle(
        0,
        0,
        CARD_WIDTH,
        CARD_HEIGHT,
        fill=PALETTE[carte["camp"]],
        rx=2,
    )
    # fond de couleur
    fond = draw.Rectangle(
        0,
        CARD_HEADER,
        CARD_WIDTH,
        CARD_HEIGHT-CARD_HEADER,
        fill=PALETTE[carte["categorie"]],
        rx=2,
    )
    # icône
    icone_path = carte.get("icone")
    if not icone_path:
        icone_path  = f"{carte.get('catgorie')}.png"
        print(icone_path)
    if (not icone_path) or (
        not os.path.exists(os.path.join("imgs", "icones", icone_path))
    ):
        icone_path = "icone.jpeg"
    icone_path = os.path.join("imgs", "icones", icone_path)
    icone = draw.Image(
        CARD_PADDING, CARD_PADDING, CARD_ICON, CARD_ICON, icone_path, embed=True )

    if carte["categorie"] == "Procédure":
        categorie_texte = f"{carte['categorie']} – {carte['etape']}"
    else:
        categorie_texte = carte["categorie"]

    titre = draw.Text(
        carte["titre"].upper(),
        POLICE_HEIGHT,
        CARD_ICON + 2* CARD_PADDING,
        CARD_PADDING+CARD_ICON/2,
        dominant_baseline='middle',
        fill="#ffffff",
        style=STYLE_TEXTE,
    )

    # image
    image_path = carte.get("image")
    if (not image_path) or (not os.path.exists(os.path.join("imgs", image_path))):
        image_path = "image.jpeg"
    image_path = os.path.join("imgs", image_path)
    image = draw.Image(
        0,
        CARD_HEADER,
        CARD_WIDTH,
        CARD_WIDTH,
        image_path,
        embed=True,
    )

    cadre_texte = draw.Rectangle(
        CARD_PADDING,
        CARD_BODY,
        CARD_WIDTH - 2 * CARD_PADDING,
        CARD_HEIGHT-CARD_BODY - CARD_PADDING,
        fill="#FFFFFF",
    )

    texte = draw.Text(
        wrapper.fill(carte["texte"]),
        POLICE_HEIGHT - 1,
        CARD_WIDTH / 2,
        CARD_BODY + CARD_PADDING,
        text_anchor='middle',
        dominant_baseline='hanging',
        fill="#FF1010",
        style=STYLE_TEXTE,
    )


    group = draw.Group()
    for element in [cadre, fond, icone, image, cadre_texte, titre, texte]:
        group.append(element)
    if carte["categorie"]=="Délit":

        fond_montant = draw.Rectangle(
        CARD_PADDING,
        DELIT_INFO,
        MONTANT_X_FIN-CARD_PADDING,
        CARD_HEADER,
        fill="#FFFFFF"
        )
        fond_prescription = draw.Rectangle(
        MONTANT_X_FIN+CARD_PADDING,
        DELIT_INFO,
        CARD_WIDTH-(MONTANT_X_FIN+2*CARD_PADDING),
        CARD_HEADER,
        fill="#FFFFFF"
        )
        montant_texte = draw.Text(
        carte["montant"],
        POLICE_HEIGHT,
        (MONTANT_X_FIN+2*CARD_PADDING)/2,
        DELIT_INFO+CARD_PADDING/2,
        text_anchor="middle",
        dominant_baseline='hanging',
        fill="#101010",
        style=STYLE_TEXTE,
        )
        prescription_texte = draw.Text(
        carte["prescription"],
        POLICE_HEIGHT,
        (MONTANT_X_FIN+CARD_WIDTH)/2,
        DELIT_INFO+CARD_PADDING/2,
        text_anchor="middle",
        dominant_baseline='hanging',
        fill="#101010",
        style=STYLE_TEXTE,
        )
        montant_legende = draw.Text(
        "Montant",
        POLICE_HEIGHT-1,
        (MONTANT_X_FIN+2*CARD_PADDING)/2,
        DELIT_INFO+CARD_HEADER-CARD_PADDING,
        text_anchor="middle",
        dominant_baseline='auto',
        fill="#101010",
        style=STYLE_TEXTE,
        )
        prescription_legende = draw.Text(
        "Prescription",
        POLICE_HEIGHT-1,
        (MONTANT_X_FIN+CARD_WIDTH)/2,
        DELIT_INFO+CARD_HEADER-CARD_PADDING,
        text_anchor="middle",
        dominant_baseline='auto',
        fill="#101010",
        style=STYLE_TEXTE,
        )

        for element in [fond_montant,montant_texte,montant_legende, fond_prescription, prescription_texte,prescription_legende]:
            group.append(element)

    return group


# Chargement des cartes et duplications selon les cardinalités

with open(FICHIER_CARTES) as f:
    cartes_uniques = json.load(f)

categories = dict()
camps = dict()
for carte in cartes_uniques.values():
    if carte["categorie"] in categories:
        categories[carte["categorie"]].append(carte["titre"].replace("\n", " "))
    else:
        categories[carte["categorie"]] = [carte["titre"].replace("\n", " ")]
    if carte["camp"] in camps:
        camps[carte["camp"]]+= 1
    else:
        camps[carte["camp"]]= 1
report = list()
report.append("# Cartes uniques")
report.append(f"Cartes uniques totales : {len(cartes_uniques)}")
report.append("## Totaux par catégorie")
for categorie, liste in categories.items():
    if categorie in CARDINALITES and CARDINALITES[categorie]>1:
        exemplaires = f"– imprimées {CARDINALITES[categorie]} fois."
    else:
        exemplaires=""
    report.append(f"- **{categorie}**: {len(liste)} ({', '.join(liste)}) {exemplaires}")

report.append(f"Un an imprimée {CARDINALITES['Un an']} fois : Deux ans imprimée {CARDINALITES['Deux ans']} fois")
report.append("## Totaux par camp")
for camp, nb in camps.items():
    report.append(f"- Camp {camp}: {nb} cartes uniques")
report.append("# Cartes imprimées")

cartes = list()
camps = dict()
for carte in cartes_uniques.values():
    exemplaires = 1
    if carte["titre"] in CARDINALITES:
        exemplaires = CARDINALITES[carte["titre"]]
    elif carte["categorie"] in CARDINALITES:
        exemplaires = CARDINALITES[carte["categorie"]]
    for i in range(0, exemplaires):
        cartes.append(carte)
        if carte["camp"] in camps:
            camps[carte["camp"]]+= 1
        else:
            camps[carte["camp"]]= 1

report.append(f"Cartes totales imprimées : {len(cartes)}")
report.append("## Totaux par camp")
for camp, nb in camps.items():
    report.append(f"- Camp {camp}: {nb} cartes uniques")

# Ajout des cartes sur les pages


number_of_pages = len(cartes) // CARDS_PER_PAGE
if len(cartes) % CARDS_PER_PAGE > 0:
    number_of_pages += 1
print(f"Nb of pages {number_of_pages}")
planches = list()
for i in range(number_of_pages):
    planches.append(draw.Drawing(PAGE_WIDTH, PAGE_HEIGHT))

for index, carte in enumerate(cartes):
    page_number = index // CARDS_PER_PAGE
    card_on_page = index % CARDS_PER_PAGE
    x_offset = (card_on_page % CARDS_PER_LINE) * CARD_WIDTH + MARGIN_WIDTH
    y_offset = (card_on_page // CARDS_PER_LINE) * CARD_HEIGHT + MARGIN_MIN_HEIGHT
    # S'il existe une bitmap de la carte, on l'utilise

    image_path = carte.get("image")
    bitmap_path = os.path.join("bitmaps", image_path)
    if (not image_path) or (not os.path.exists(bitmap_path)):
        svg_group = to_svg(carte)
    else:
        svg_group = from_bitmap(bitmap_path)
    # print(svg_group.transform)
    in_pos = draw.Group(transform=f"translate({x_offset},{y_offset})")
    in_pos.append(svg_group)
    planches[page_number].append(in_pos)

print(f"Nombre de cartes imprimées : {len(cartes)}")

# Ajout des pointillés de découpe
grille = draw.Group()

for nb in range(CARDS_PER_LINE + 1):
    ligne_v = draw.Line(
        MARGIN_WIDTH + nb * CARD_WIDTH,
        0,
        MARGIN_WIDTH + nb * CARD_WIDTH,
        PAGE_HEIGHT,
        stroke="black",
        stroke_width=LARGEUR_GRILLE,
        stroke_dasharray=1,
        fill="none",
    )
    grille.append(ligne_v)

for nb in range(LINES_PER_PAGE + 1):
    ligne_h = draw.Line(
        0,
        MARGIN_MIN_HEIGHT + nb * CARD_HEIGHT,
        PAGE_WIDTH,
        MARGIN_MIN_HEIGHT + nb * CARD_HEIGHT,
        stroke="black",
        stroke_width=LARGEUR_GRILLE,
        stroke_dasharray=1,
        fill="none",
    )
    grille.append(ligne_h)

# Crée les fichiers de sortie

merger = PdfWriter()
for i in range(number_of_pages):
    planches[i].append(grille)
    print(f"Saving planche {i}")
    planches[i].set_pixel_scale(4)  # Set number of pixels per geometry unit
    planches[i].save_svg(f"build/NCartes{i}.svg")
    pdf_name = f"build/NCartes{i}.pdf"
    cairosvg.svg2pdf(url=f"build/NCartes{i}.svg", write_to=pdf_name)
    merger.append(pdf_name)
merger.write("Cartes_pour_test.pdf")
merger.close()
report_texte = "\n".join(report)
print(report_texte)
with open(FICHIER_RESUME,'w') as f:
    f.write(report_texte)

