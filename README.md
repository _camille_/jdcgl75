# jdcgl75

Un script pour générer des cartes.

## Prérequis :

- Le gestionnaire de dépendances Python [Poetry](https://python-poetry.org/)
- Python >= 3.10

## Fichiers

- `generate.py` génère ou modifie un fichier cartes.json contenant les 
caractéristiques des cartes individuelles et un fichier cartes_repetees.json 
contenant les cartes répétées selon les cardinalités indiquées.
- `cartes_from_json.py` génère un `Cartes_pour_test.pdf` à partir du fichier 
`cartes.json`.
## Utilisation
- Cloner le dépôt : `git clone https://framagit.org/_camille_/jdcgl75.git`
- Installer les dépendances : `cd jdcgl75 && poetry install`
- Activer l'environnement virtuel : `poetry shell`
- Lancer le script pour générer le PDF `python cartes_from_json.py`
- Vous pouvez modifier le contenu des cartes dans le fichier `cartes.json` et le
nombre de fois que chacune est répétée dans la constante `CARDINALITES` du 
fichier `cartes_from_json.py`.
# Licence
Tous les fichiers à l'exception du contenu (textes et  éléments graphiques) : 
(c) GL 75 - Unlicense
